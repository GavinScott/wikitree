﻿using System.Collections.Generic;

namespace WikiTree {

    // Represents a Person in a family tree
    class Person {
        public readonly string Name;
        public readonly string Url;
        public readonly HashSet<Person> Parents;
        public readonly HashSet<Person> Children;
        public readonly HashSet<Person> Spouses;
        private readonly bool isHead;

        public Person(string name, string url, bool isHead = false) {
            this.Name = name;
            this.Url = url;
            this.Parents = new HashSet<Person>();
            this.Children = new HashSet<Person>();
            this.Spouses = new HashSet<Person>();
            this.isHead = isHead;
        }

        public void AddChild(Person child) {
            child.Parents.Add(this);
            Children.Add(child);
        }

        public void AddSpouse(Person spouse) {
            spouse.Spouses.Add(this);
            Spouses.Add(spouse);
        }

        public void AddParent(Person parent) {
            parent.Children.Add(this);
            Parents.Add(parent);
        }

        public override bool Equals(object obj) {
            return obj is Person && ((Person)obj).Url == this.Url;
        }

        public override int GetHashCode() {
            return Url.GetHashCode();
        }

        public string ToString(bool html) {
            string s = Name;
            if (html && isHead) {
                s = "<font color=\"red\">" + Name + "</font>";
            }
            foreach (Person spouse in Spouses) {
                s += " + " + spouse.Name;
            }
            return s;
        }
    }
}
