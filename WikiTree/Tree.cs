﻿using MoreLinq;
using System.Collections.Generic;

namespace WikiTree {

    // Represents a Family Tree
    class Tree {
        private Person head;
        private readonly string startName;
        private readonly string HEADER = "Family Tree: ";

        // Constructs a tree with an initial member
        public Tree(Person head) {
            this.head = head;
            this.startName = head.Name;
        }

        // Searches the Tree for a person with the given name
        public Person Find(string url) {
            return search(this.head, url, new HashSet<string>());
        }

        // Checks if a person with the given name is in the tree
        public bool Contains(string url) {
            return Find(url) != null;
        }

        private Person search(Person cur, string url, HashSet<string> visited) {
            Person res = null;
            if (cur.Url == url) {
                return cur;
            }
            foreach (Person child in cur.Children) {
                if (!visited.Contains(child.Url)) {
                    visited.Add(child.Url);
                    res = search(child, url, visited);
                }
            }
            
            if (res == null) {
                foreach (Person parent in cur.Parents) {
                    if (!visited.Contains(parent.Url)) {
                        visited.Add(parent.Url);
                        res = search(parent, url, visited);
                    }
                }
            }
            if (res == null) {
                foreach (Person spouse in cur.Spouses) {
                    if (!visited.Contains(spouse.Url)) {
                        visited.Add(spouse.Url);
                        res = search(spouse, url, visited);
                    }
                }
            }
            
            return res;
        }

        // Returns the total number of people in the tree
        public int Count() {
            return count(this.head, new HashSet<string>());
        }

        private int count(Person cur, HashSet<string> visited) {
            int sum = 1;
            foreach (Person child in cur.Children) {
                if (!visited.Contains(child.Url)) {
                    visited.Add(child.Url);
                    sum += count(child, visited);
                }
            }
            foreach (Person parent in cur.Parents) {
                if (!visited.Contains(parent.Url)) {
                    visited.Add(parent.Url);
                    sum += count(parent, visited);
                }
            }
            foreach (Person spouse in cur.Spouses) {
                if (!visited.Contains(spouse.Url)) {
                    visited.Add(spouse.Url);
                    sum += count(spouse, visited);
                }
            }
            return sum;
        }

        // Rearranges the tree so that oldest person is the head
        public Tree Rearrange() {
            this.head = findHighestAncestor(this.head).Person;
            return this;
        }

        private static Ancestor findHighestAncestor(Person cur, int height = 0) {
            if (cur.Parents.Count == 0) {
                return new Ancestor(cur, height);
            }
            List<Ancestor> parents = new List<Ancestor>();
            foreach (Person parent in cur.Parents) {
                parents.Add(findHighestAncestor(parent, height + 1));
            }
            return parents.MaxBy(p => p.Distance);
        }

        // Returns a string representation of the tree, indented to indicate levels
        public override string ToString() {
            return HEADER + this.startName + "\n\n" + str(head);
        }

        // Returns an HTML representation of the tree, indented to indicate levels
        public string ToHTML() {
            string s = "<h1>" + HEADER + this.startName + "</h1><br/><br/>" + "<FONT FACE=\"courier\">" + str(head, 0, true);
            return s + "<br/><br/>Total tree size (not all shown): " + Count() + "</FONT>";
        }

        private string str(Person cur, int indent = 0, bool html = false) {
            string s = "";
            string br = "\n";
            string tab = "   ";
            if (html) {
                br = "<br/>";
                tab = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
            }
            for(int i = 0; i < indent; i++) {
                s += tab;
            }
            s += cur.ToString(html) + br;
            foreach (Person p in cur.Children) {
                s += str(p, indent + 1, html);
            }
            return s;
        }

        private class Ancestor {
            public readonly Person Person;
            public readonly int Distance;
            public Ancestor(Person person, int distance) {
                this.Person = person;
                this.Distance = distance;
            }
        }
    }
}