﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace WikiTree {
    
    // Contains the main for WikiTree
    class Program {
        private static readonly string WIKI_URL = "https://en.wikipedia.org";

        // Runs the program, building a tree for the person specified in the single argument to args
        static void Main(string[] args) {
            // verify input
            if (args.Length != 1) {
                Console.WriteLine("Please specify exactly one person (ex. \"Abraham Lincoln\")");
                Environment.Exit(-1);
            }
            // get starting point
            string start = Regex.Replace(args[0], @"\s+", "_");
            Console.WriteLine("Building family tree...");
            string url = buildURL(start);
            // start building tree
            Tree tree = new Tree(new Person(args[0], url, true));
            handlePage(tree, args[0], url);
            tree.Rearrange();
            // print & save output
            Console.WriteLine("\n" + tree.ToString());
            Console.WriteLine("Total tree size (not all shown): " + tree.Count() + "\n");
            // save HTML file
            try {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\tree.html";
                Console.WriteLine("tree.html written to " + path);
                File.WriteAllText(path, tree.ToHTML());
            } catch (Exception ex) {
                Console.WriteLine("Error writing tree.html to desktop" + ex);
            }
        }

        // Builds a valid url from a name or an href
        private static string buildURL(string orig) {
            if (orig.Contains(".org")) {
                return orig;
            }
            if (!orig.Contains("/wiki/")) {
                orig = "/wiki/" + orig;
            }
            return WIKI_URL + orig;
        }

        // Handles visiting a person's wiki page, adding them to the tree.
        // Also visits sub-pages (children, etc.)
        private static void handlePage(Tree tree, string name, string url) {
            // Visit wiki page
            var doc = new HtmlAgilityPack.HtmlDocument();
            HtmlAgilityPack.HtmlNode.ElementsFlags["br"] = HtmlAgilityPack.HtmlElementFlag.Empty;
            doc.OptionWriteEmptyNodes = true;

            try {
                var webRequest = HttpWebRequest.Create(url);
                Stream stream = webRequest.GetResponse().GetResponseStream();
                doc.Load(stream);
                stream.Close();
            } catch (System.UriFormatException e) {
                Console.WriteLine("Bad URL: " + url, e);
                throw;
            } catch (System.Net.WebException e) {
                Console.WriteLine("Connection Error: " + url, e);
                throw;
            }

            doField(tree, doc, url, "Children", (Person p) => tree.Find(url).Children.Add(p));
            doField(tree, doc, url, "Spouses", (Person p) => tree.Find(url).AddSpouse(p));
            doField(tree, doc, url, "Spouse(s)", (Person p) => tree.Find(url).AddSpouse(p));
            doField(tree, doc, url, "Parents", (Person p) => tree.Find(url).AddParent(p));
            doField(tree, doc, url, "Parent(s)", (Person p) => tree.Find(url).AddParent(p));
        }

        private delegate void AddFunction(Person person);

        private static void doField(Tree tree, HtmlAgilityPack.HtmlDocument doc, string url, string fieldName, AddFunction func) {
            // find people
            var people = doc.DocumentNode
                .Descendants("td")
                .Where(d => d.ParentNode.InnerText.StartsWith("\n" + fieldName)
            );
            // visit and add people
            foreach (HtmlAgilityPack.HtmlNode row in people) {
                foreach (HtmlAgilityPack.HtmlNode child in row.Descendants()) {
                    if (child.Attributes.Contains("href")) {
                        Person cur = tree.Find(buildURL(url));
                        if (cur != null) {
                            string newURL = buildURL(child.Attributes["href"].Value);
                            bool doPage = !tree.Contains(newURL);
                            // add person to tree
                            func(new Person(child.InnerText, newURL));
                            if (doPage) {
                                // visit person's page
                                Console.WriteLine(fieldName + " of " + cur.Name + ": " + newURL);
                                handlePage(tree, child.InnerText, newURL);
                            }
                        }
                    }
                }
            }
        }
    }
}