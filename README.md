# README #

This is a C# web scraper that pulls data from Wikipedia to build family trees for someone.

Usage: WikiTree.exe "Abraham Lincoln"

The given name must match the name on the Wikipedia page (Lincoln's page is [https://en.wikipedia.org/wiki/Abraham_Lincoln](https://en.wikipedia.org/wiki/Abraham_Lincoln), so the given name should be "Abraham Lincoln")